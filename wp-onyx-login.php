<?php
// Load WordPress
require( dirname( __FILE__ ) . '/wp-load.php' );
// Compute the hash we are expecting using the provided expiry time
$hash = hash_hmac( 'sha256', $_GET['expiry'], AUTH_KEY);
// Check if the computed hash matches the provided hash
if ($_GET['hash'] == $hash && $_GET['expiry'] > time()) {
  // If the key exists in the cache, it has already been used, so reject it
  if (wp_cache_get($_GET['hash'], 'onyx-login')) {
    echo("Login Error (hash)");
    die;
  } else {
    // Add the used key to the cache
    wp_cache_set($_GET['hash'], "USED", 'onyx-login');
    if ($_GET['user_id']) {
      // Find a user with the provided ID.
      $user = get_user_by('id', $_GET['user_id']);
    } 
    if (!$user) {
      // Find the first admin user
      $user = get_users('role=administrator')[0];
    }      
    // Log in the user
    wp_set_current_user($user->ID, $user->user_login);
    wp_set_auth_cookie($user->ID);
    do_action( 'wp_login', $user->user_login, $user );
    // Redirect to admin URL
    header('Location: ' . admin_url() );
    die;
  }
}
// Fall back to printing an error
echo("Login Error (generic)");
die;

